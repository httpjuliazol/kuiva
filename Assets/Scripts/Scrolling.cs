﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{
    private float startposx;
    public float length;
    public GameObject cam;
    public float parallaxEffects;
    public float posy;
    public float predelsee;



    void Start()
    {
        startposx = transform.position.x;
      // Debug.Log(startposx);

    }
       
    // Update is called once per frame
    void Update()
    {
       // float temp = (cam.transform.position.x * (1 - parallaxEffects));
      // Debug.Log(temp);
       float distx = (cam.transform.position.x * parallaxEffects);
    
        transform.position = new Vector3(startposx + distx, Mathf.Clamp(cam.transform.position.y,2,predelsee) - posy, transform.position.z);

       /* if (temp > startposx + length)
        { startposx += length; }
        else if (temp < startposx - length) { startposx -= length; }*/



    }
}


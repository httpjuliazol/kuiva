﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MenuControls;

public class PlayerController : MonoBehaviour
{
    // Публичные конфигурации
    [SerializeField] float moveSpeed = 0f;
    [SerializeField] float JumpSpeed = 0f;
    [SerializeField] float AngularDrag = 0f;
    [SerializeField] AudioClip[] stepssound;
    [SerializeField] AudioClip[] jumpsound;
    [SerializeField] AudioClip[] ground;
    [SerializeField] AudioClip death;
  



    //Состояния
    private bool isKuiva = false;
    private bool isJump = false;
    private float Stepstime = 5;
    private float timeout;
    private bool ismusicland = false;
    private bool isDeath = false;


    //создание объекта
    Animator myanimation;
    Rigidbody2D myrigidbody;
    CapsuleCollider2D bodycollider;
    BoxCollider2D feetcollider;
    CircleCollider2D circletrigger;

    // circletrigger.IsTouchingLayers(LayerMask.GetMask("Ground"))
    void Start()
    {
        myanimation = GetComponent<Animator>();
        myrigidbody = GetComponent<Rigidbody2D>();
        bodycollider = GetComponent<CapsuleCollider2D>();
        feetcollider = GetComponent<BoxCollider2D>();
        circletrigger = GetComponent<CircleCollider2D>();

        Debug.Log(PlayerPrefs.GetString("Left"));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Run();
       if (!isKuiva)
        {
            Jump();
        }
        Flip();
        Kuiva();
        Land();
        Death();

    }

    void Run()
    {
        float move = Input.GetAxis("Horizontal");

        if (Input.GetButton("Fire1") || Input.GetButtonUp("Fire1"))
        {
            myrigidbody.velocity = new Vector2(move / 3 * moveSpeed, myrigidbody.velocity.y);
        }


        else 
        {
            //0.1614743
            timeout += Time.deltaTime;
            myrigidbody.velocity = new Vector2(move * moveSpeed, myrigidbody.velocity.y);
            myanimation.SetFloat("MoveSpeed", Mathf.Abs(move));
            
            if (Mathf.Abs(move) > 0 && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && timeout > 0.3)
            {
                AudioSource.PlayClipAtPoint(stepssound[Random.Range(0, stepssound.Length)], Camera.main.transform.position);
                timeout = 0;
            }

        }
    }

    /* IEnumerable SoundStep()
     {
         AudioSource.PlayClipAtPoint(stepssound,  Camera.main.transform.position);
         yield return new WaitForSeconds(StepTime);
     }*/

    void Flip()
    {
        if (Mathf.Abs(myrigidbody.velocity.x) > 0)
        {
            transform.localScale = new Vector2(Mathf.Sign(myrigidbody.velocity.x) * 2, 2f);
        }
    }

    void Kuiva()
    {
        float move = Input.GetAxis("Horizontal");
        if (Input.GetButton("Fire1"))
        {
            myanimation.SetBool("isKuiva", true);
            isKuiva = true;
            myanimation.SetFloat("MoveSpeed", Mathf.Abs(move));

     

            /* if (Input.GetButtonUp("Fire1"))
             {
                 for (timeout = 100; timeout > 0; timeout--)
                 {
                     Debug.Log("Работает");
                     myrigidbody.velocity = new Vector2(0, myrigidbody.velocity.y);
                 }


             }*/
        }



        else { myanimation.SetBool("isKuiva", false);
            isKuiva = false;
        }

    }

    void Jump()
    {

        if (Input.GetButtonDown("Jump") && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            myanimation.SetBool("isJump", true);
            myrigidbody.AddForce(new Vector2(0f, JumpSpeed), ForceMode2D.Impulse);
            myanimation.SetFloat("JumpSpeed", myrigidbody.velocity.y);
            AudioSource.PlayClipAtPoint(jumpsound[Random.Range(0, jumpsound.Length)], Camera.main.transform.position);
            myanimation.SetBool("isGround", false);

        }

        if (feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
          {
             
              myanimation.SetFloat("JumpSpeed", 0f);
              myanimation.SetBool("isGround", true);
              myanimation.SetBool("isLand", false);
        }

    }

    void Land()
    {
       
        if (myrigidbody.velocity.y < 4 && !feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && !circletrigger.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            myanimation.SetBool("isLand", true);
            myanimation.SetBool("isGround", false);
            myanimation.SetFloat("JumpSpeed", myrigidbody.velocity.y);
            myanimation.SetBool("isJump", false);
            ismusicland = true;
            if (myrigidbody.velocity.y < -25)
            {
                Debug.Log("DEATH");
                myanimation.SetBool("isDeath", true);
                ismusicland = false;
            }
        }

        if (ismusicland == true && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground"))) {
            AudioSource.PlayClipAtPoint(ground[Random.Range(0, ground.Length)], Camera.main.transform.position);
            ismusicland = false;
        }
    }

    void Death()
    {
        if(myanimation.GetBool("isDeath") && myanimation.GetBool("isGround") && !isDeath){
            Debug.Log("HAPPY END");
            //audiosourse.PlayOneShot(death);
            AudioSource.PlayClipAtPoint(death, Camera.main.transform.position);
            isDeath = true;

        }
    }

    /* private void Death()
    {
        if (feet.IsTouchingLayers(LayerMask.GetMask("Traps")))
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
            isAlive = !isAlive;
            anim.SetFloat("Speed", 0);
        }
    }

    //Debug.Log(myanimation.GetBool("isJump"));
    /*Debug.Log(myrigidbody.velocity.y);

    if (feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    {
        myanimation.SetBool("isJump", false);
    }

    if (!feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    { return; }

    if (Input.GetButtonDown("Jump") && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    {
        if 
        myanimation.SetBool("isJump", true);
        myanimation.SetFloat("JumpSpeed", myrigidbody.velocity.y);

        // myrigidbody.velocity += new Vector2(0f, JumpSpeed);
        myrigidbody.AddForce(new Vector2(0f, JumpSpeed), ForceMode2D.Impulse);

    }*/

}

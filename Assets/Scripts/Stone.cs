﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    // Start is called before the first frame update

    BoxCollider2D stone;
    Rigidbody2D rbstone;
    [SerializeField] AudioClip music;
    private float timeout;
    void Start()
    {
        stone = GetComponent<BoxCollider2D>();
        rbstone = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (stone.IsTouchingLayers(LayerMask.GetMask("Player")) && Input.GetButton("Fire1"))
        {
            rbstone.mass = 2;
            //rbstone.bodyType = RigidbodyType2D.Dynamic; 
          /*  timeout += Time.deltaTime;
            if (timeout > 0.3)
            {
                AudioSource.PlayClipAtPoint(music, Camera.main.transform.position);
                timeout = 0;
            }*/
        }

        else
        {
            rbstone.mass = 999999;
            //rbstone.bodyType = RigidbodyType2D.Static;
        } 
    }
}
